Title: People
Tags: HPGMG, people
Summary:

* [**Mark Adams** (LBL)](http://crd.lbl.gov/departments/applied-mathematics/ANAG/about/staff/mark-adams/) Multigrid algorithms and performance analysis.
* [**Jed Brown** (ANL & CU)](https://jedbrown.org) HPGMG-FE Finite-element implementation, multigrid algorithms, performance analysis
* [**John Shalf** (LBL)](http://crd.lbl.gov/about/staff/computer-science/john-shalf/) Hardware roadmaps, advisor
* [**Brian Van Straalen** (LBL)](http://crd.lbl.gov/departments/applied-mathematics/ANAG/about/staff/brian-van-straalen/)
* [**Erich Strohmaier** (LBL)](http://crd.lbl.gov/about/staff/cds/ftg/erich-strohmaier) Advisor
* [**Sam Williams** (LBL)](http://crd.lbl.gov/about/staff/computer-science/par/samuel-williams/) HPGMG-FV Finite-volume implementation, performance engineering
