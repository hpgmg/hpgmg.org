Title: SC14 BoF
Category:
Tags: community
Date: 2014-11-19
Summary: SC14 BoF

Our [SC14 HPGMG birds of a feather (BoF) session](http://sc14.supercomputing.org/schedule/event_detail?evid=bof146) took place today.
Mark Adams gave an introduction to our benchmarking effort, Sam Williams gave details about the finite-volume implementation and performance results, and Jed Brown highlighted characteristics of the finite-element implementation and the concept of "dynamic range" or "performance versatility".
Following these presentations by the primary developers, Mitsuhisa Sato from RIKEN AICS presented recent experience on the K computer and Muthu Baskaran of Reservoir Labs presented recent work on task-based programming models.
The slides for all talks [are available](/static/SC14_HPGMG_BoF.pptx).

The BoF concluded with audience questions for the panel, notably

* **Q:** What about nonlinear problems such as Poisson-Boltzmann? **A:** Nonlinearities create a scale dependence, but we want a scale-independent benchmark (where convergence does not depend on resolution) and do not believe that a weak nonlinearity changes the machine characterization.  See [further discussion](/fe/#linear-or-nonlinear).

* **Q:** Which MPI primitives are used?  **A:** Both implementations use nonblocking point-to-point by default.  HPGMG-FE has an option to use one-sided communication, though we have not found it to be faster.

* A paper critiquing HPCG was discussed: [Marjanović, Gracia, and Glass, *Performance modeling of the HPCG benchmark*](/static/MarjanovicGraciaGlass-PerformanceModelHPCG-2014.pdf).

We encourage interested parties to continue discussion on the [hpgmg-forum mailing list](/lists/listinfo/hpgmg-forum).
