Title: HPGMG-0.3
Category: Results
Tags: release
Date: 2015-09-15
Summary: HPGMG-0.3 release announcement.

Version 0.3 of HPGMG-FV was released on July 17, 2015.  Unlike the previous 2nd order version, v0.3 is 4th order, cell-averaged, finite volume, full multigrid solver for the variable coefficient
Laplacian with high-order dirichlet boundary conditions.  Additionally, interpolation was modified to preserve volume-averaged properties and order, and the default smoother was modified to be an
out-of-place GSRB(3).  Combined, the solver provides the discretization error and up to 9 digits on the relative residual in a single F-Cycle.

Compared to the older 2nd order implementation, for a fixed number of cells, the 4th order stencil will perform roughly 4x the FP operations and send 3x the messages of 2x the volume.  However, as
it requires no extra DRAM data movement per stencil, it is substantially more compute and network intensive.

The benchmark interface is as before with the caveat that it will automatically benchmark three problem sizes (N, N/8, and N/64) in order to estimate the system's dynamic range and verify 4th order
properties.

HPGMG rankings will now be based on v0.3.  See [detailed submission guidelines and porting suggestions](http://crd.lbl.gov/departments/computer-science/performance-and-algorithms-research/research/hpgmg/submission/) for further details.

