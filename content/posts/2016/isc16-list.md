Title: ISC'16 List
Category: Results
Date: 2016-06-21
Summary: ISC'16 presentation and June 2016 ranking list

Mark Adams gave a [presentation](/static/HPGMG-ISC16.pdf) on HPGMG at ISC'16 today.  This presentation includes new results using the 4th order HPGMG-FV with the CPU implementation and a new CUDA implementation for GPU-equipped machines as well as an analysis of strong scalability and a simple metric that could be used to reward versatility.

The [June 2016 Ranking List](https://crd.lbl.gov/departments/computer-science/PAR/research/hpgmg/results/results-201606/) includes the 2h and 4h performance (testing strong scaling).  This figure shows total performance (measured as Degrees of Freedom per second) versus solve time for several large machines.  Each line shows the performance for a particular machine as the problem size is varied.  We can see, for instance, that while Mira is faster on sufficiently large problem sizes (sufficiently generous solve time requirements), Hazel Hen shows its versatility by sustaining higher performance across a broader range including very fast solve times.  Consequently, many scientific and engineering applications that desire performance in the upper left portion of this graph will find Hazel Hen to be a more capable machine.

![Performance versus time](/static/isc16-strong-scaling.png)

See [detailed submission guidelines](/submission) if you would like to contribute to this list.

