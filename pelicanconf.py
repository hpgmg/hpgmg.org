#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'HPGMG Developers'
SITENAME = 'HPGMG'
SITEURL = 'https://hpgmg.org'

TIMEZONE = 'America/Denver'

DEFAULT_LANG = 'en'

PAGE_URL = '{slug}'
PAGE_SAVE_AS = '{slug}/index.html'
ARTICLE_URL = '{date:%Y}/{date:%m}/{date:%d}/{slug}'
ARTICLE_SAVE_AS = '{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html'

DISPLAY_PAGES_ON_MENU = True
DISPLAY_CATEGORIES_ON_MENU = False
USE_FOLDER_AS_CATEGORY = False

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None

PORTRAIT_IMG = 'images/HPGMG-logo2.png'
PORTRAIT_ALT = "HPGMG Logo"

DEFAULT_PAGINATION = False

PLUGINS = ['render_math',
           #'bibtex',
           'extract_toc']
PLUGIN_PATHS = ['./pelican-plugins']
MD_EXTENSIONS = ['codehilite(css_class=highlight)', 'extra', 'headerid', 'toc']
STATIC_PATHS = ['theme/images', 'images', 'files', 'static']

LANDING_PAGE_ABOUT = dict(title='High-performance Geometric Multigrid',
                          details='landing-page-details.html')

THEME = 'hpgmg-bootstrap'
BOOTSTRAP_THEME = 'spacelab'
PYGMENTS_STYLE = 'default'

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
